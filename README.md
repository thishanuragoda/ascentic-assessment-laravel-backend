# Lavravel backend

## Project setup

After downloading (cloning) the repository, navigate to root folder in terminal and execute following command to download the dependencies.

```bash
composer install
```

Next create or duplicate the `env.example` and rename the file as `.env` and update the database connection values.

Next execute following command to setup database and create test user accounts.

```bash
php artisan migrate --seed
```

Next execute following command to generate `laravel/passwort` password grant `client_id` and `client_secret` which are required in frontend.

```bash
php artisan passport:install
```

Next execute following command to run the built-in development server:
(If different port is used, frontend api endpoint must also reflect the same port value)

```bash
php artisan serve --port=8000
```


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
