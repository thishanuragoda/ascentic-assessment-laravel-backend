<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'name' => "Administrator",
            'email' => 'admin@app.com',
            'password' => bcrypt('password'),
        ]);

        if ($admin->wasRecentlyCreated) {
            $admin->update(['is_admin' => true ]);
        }

        User::create([
            'name' => "User",
            'email' => 'user@app.com',
            'password' => bcrypt('password'),
        ]);

//        \App\Models\User::factory(10)->create();
    }
}
