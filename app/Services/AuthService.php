<?php


namespace App\Services;


use App\Repositories\UserRepositoryInterface;

class AuthService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $attributes
     */
    public function register(array $attributes = [])
    {
        $attributes['password'] = bcrypt($attributes['password']);
        $attributes['email_verified_at'] = now();
        return $this->userRepository->create($attributes);
    }
}
