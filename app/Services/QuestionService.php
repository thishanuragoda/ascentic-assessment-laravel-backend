<?php


namespace App\Services;


use App\Models\User;
use App\Repositories\QuestionRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

class QuestionService
{
    private $questionRepository;

    public function __construct(QuestionRepositoryInterface $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * Retrieves all question records filtered by list of criteria, with relations
     * @param array $criteria
     * @param array $with
     * @return Collection
     */
    public function getAllQuestions(array $criteria = [], array $with =['user', 'comments']): Collection
    {
        return $this->questionRepository->all([ 'filters' => $criteria, 'with' => $with, 'order' => true ]);
    }


    /**
     * @param User $user
     * @param array $attributes
     * @return mixed
     */
    public function create(User $user, array $attributes = [])
    {
        if($user->is_admin) {
            $attributes['approved'] = true;
        }
        return $this->questionRepository->create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return bool
     */
    public function update(int $id, array $attributes = []): bool
    {
        return $this->questionRepository->update($id, $attributes);
    }

    /**
     * @param $id
     * @return bool
     */
    public function approve($id): bool
    {
        return $this->questionRepository->update($id, ['approved' => true]);
    }

    /**
     * @param $id
     * @return bool
     * @throws ValidationException
     */
    public function delete($id): bool
    {
        return $this->questionRepository->delete($id);
    }
}
