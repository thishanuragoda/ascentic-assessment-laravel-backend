<?php


namespace App\Services;


use App\Http\Requests\CommentRequest;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CommentService
{
    private $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @param array $filters
     * @return Collection
     */
    public function getAll(array $filters): Collection
    {
        return $this->commentRepository->all();
    }

    public function create(CommentRequest $request)
    {
        $attributes = $request->validated();
        return $this->commentRepository->create($attributes);
    }

    public function update(int $id, CommentRequest $request): Model
    {
        $attributes = $request->validated();
        return $this->commentRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        return $this->commentRepository->delete($id);
    }

}
