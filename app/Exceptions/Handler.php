<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        $ErrorID = time();
        $ErrorBag = [
            'success' => false,
            'code' => $e->getCode(),
            'line' => $e->getLine(),
            'file' => $e->getFile(),
            'id' => $ErrorID
        ];
        if ($e instanceof ModelNotFoundException && $request->expectsJson()) {
            $ErrorBag['message'] = "Record not found inside";
            Log::error(sprintf('ERROR [%d]', $ErrorID), $ErrorBag);
            return response()->json($ErrorBag, 422);
        }
        if ($e instanceof AuthorizationException && $request->expectsJson()) {
            $ErrorBag['message'] = "Unauthorized";
            Log::error(sprintf('ERROR [%d]', $ErrorID), $ErrorBag);
            return response()->json($ErrorBag, 422);
        }

        if ($e instanceof ValidationException && $request->expectsJson()) {
            $ErrorBag['message'] = "Validation failed";
            Log::error(sprintf('ERROR [%d]', $ErrorID), $ErrorBag);
            return response()->json($ErrorBag, 422);
        }

        $ErrorBag['message'] = "Unauthorized";
        $ErrorBag['customError'] = true;
        Log::error(sprintf('ERROR [%d]', $ErrorID), $ErrorBag);
        return response()->json($ErrorBag, 422);
    }

}
