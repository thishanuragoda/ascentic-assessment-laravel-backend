<?php


namespace App\Repositories;


interface QuestionRepositoryInterface
{
    public function update(int $id, array $attributes = []): bool;
}
