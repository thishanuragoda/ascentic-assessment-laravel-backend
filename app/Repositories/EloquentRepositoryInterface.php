<?php


namespace App\Repositories;


use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;

    /**
     * @param array|null $criteria
     * @return Collection
     */
    public function all(array $criteria = []): Collection;


    /**
     * @param array $attributes
     */
    public function create(array $attributes);


    /**
     * @param int $id
     * @param array $attributes
     */
    public function update(int $id, array $attributes);

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool;

    /**
     * @param $field
     * @param $value
     * @param $operator
     * @return mixed
     */
    public function where($field, $value, $operator);
}
