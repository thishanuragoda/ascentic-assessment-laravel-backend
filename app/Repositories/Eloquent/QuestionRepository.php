<?php


namespace App\Repositories\Eloquent;


use App\Models\Question;
use App\Repositories\QuestionRepositoryInterface;

class QuestionRepository extends BaseRepository implements QuestionRepositoryInterface
{
    public function __construct(Question $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return bool
     */
    public function update(int $id, array $attributes = []): bool
    {
        $question = $this->model->where('id', $id)->first();
        return $question->update($attributes);
    }
}
