<?php


namespace App\Repositories\Eloquent;


use App\Models\Comment;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Model
     */
    public function update(int $id, array $attributes = []): Model
    {
        $question = $this->model->find($id);
        $question->update($attributes);
        return $question->fresh();
    }
}
