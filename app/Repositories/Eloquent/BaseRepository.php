<?php


namespace App\Repositories\Eloquent;

use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    public $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * Search by given fields (text, user) and status
     * when 'filters' parameters are provided
     * @param array|null $criteria
     * @return Collection
     */
    public function all(array $criteria = []): Collection
    {
        $query = $this->model->newQuery();

        // Handle requests with search
        if(isset($criteria['filters'])) {
            // Loop through the filters
            foreach ($criteria['filters'] as $key => $value) {
                // If value not null/empty
                if($value) {
                    $query->where($key, $value);
                }
            }
        }

        // Load necessary relationships
        if (isset($criteria['with']))$query->with($criteria['with']);

        // Order the question records to show the latest one first
        if (isset($criteria['order'])){
            $query->orderByDesc('id');
        }

        return $query->get();
    }

    /**
     * @param array $attributes
     *
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     */
    public function update(int $id, array $attributes)
    {
        $record = $this->model->find($id);
        return $record->update($attributes);
    }


    /**
     * @param $id
     * @return bool
     * @throws ValidationException
     */
    public function delete($id): bool
    {
        $record = $this->model->findOrFail($id);
        return $record->delete();
    }

    /**
     * @param $field
     * @param $value
     * @param string $operator
     * @return mixed
     */
    public function where($field, $value, $operator = "=")
    {
        return $this->model->where($field, $value, $operator);
    }
}
