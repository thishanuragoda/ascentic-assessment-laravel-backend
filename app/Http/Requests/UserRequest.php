<?php

namespace App\Http\Requests;


class UserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2',
            'password' => 'required|string|min:6|confirmed',
            'email' => 'required|email|max:255|unique:users,email',
            'email_verified_at' => 'nullable|datetime'
        ];
    }
}
