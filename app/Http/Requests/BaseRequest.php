<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();


    /**
     * Sends error response when the request is unauthorized
     */
    public function failedAuthorization() {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => [],
            'message' => "Authorization failed"
        ], 422));
    }

    /**
     * Sends error response when the request validation fails
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors(),
            'message' => "Validation failed"
        ], 422));
    }
}
