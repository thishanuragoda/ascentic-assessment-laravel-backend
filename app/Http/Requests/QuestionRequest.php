<?php

namespace App\Http\Requests;


class QuestionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'topic' => 'required|string|min:3|max:255',
            'description' => 'required|string|min:3'
        ];

        return $rules;
    }

    /***
     * Customize the passed request values after validation
     * to include the logged in user's id as creating user_id
     * */
    public function validated()
    {
        return array_merge(parent::validated(), [
            'user_id' => auth()->user()->id
        ]);
    }
}
