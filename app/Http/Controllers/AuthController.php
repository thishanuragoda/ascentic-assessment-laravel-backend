<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Services\AuthService;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    use ResponseTrait;

    private $service;

    /**
     * AuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->service = $authService;
    }

    /**
     * Registers a new user using email, name, password
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function register(UserRequest $request): JsonResponse
    {
        $user = $this->service->register($request->validated());
        return $this->apiResponse(new UserResource($user), "success");
    }

    // Retrieves the authenticated user using request headers, and returns
    public function user(Request $request)
    {
        return $this->apiResponse(new UserResource($request->user()), "success");
    }

    // Loops through 'oauth_access_tokens' tokens for the authenticated user and deletes them,
    // to revoke all previous logins
    public function logout(Request $request)
    {
        DB::table('oauth_access_tokens')->where('user_id', $request->user()->id)->delete();
        return $this->apiResponse([], "success");
    }
}
