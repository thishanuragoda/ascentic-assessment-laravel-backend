<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Services\CommentService;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    use ResponseTrait;

    private $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * Retrieves all comments filtered by query parameters, and user relationship
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $comments = $this->commentService->getAll($request->all());
        return $this->apiResponse(CommentResource::collection($comments), "success");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentRequest  $request
     * @return JsonResponse
     */
    public function store(CommentRequest $request): JsonResponse
    {
        $comment = $this->commentService->create($request);
        return $this->apiResponse(new CommentResource($comment), 'Comment saved successfully.');
    }

    /**
     * Fetches the comment record via route model binding
     *
     * @param Comment $comment
     * @return JsonResponse
     */
    public function show(Comment $comment): JsonResponse
    {
        return $this->apiResponse(new CommentResource($comment), 'Comment saved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CommentRequest  $request
     * @param Comment $comment
     * @return JsonResponse
     */
    public function update(CommentRequest $request, Comment $comment): JsonResponse
    {
        $this->authorize('update', $comment);
        $comment = $this->commentService->update($comment->id, $request);
        return $this->apiResponse(new CommentResource($comment), 'Comment updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Comment $comment): JsonResponse
    {
        $this->authorize('delete', $comment);
        $this->commentService->delete($comment->id);
        return $this->apiResponse([ 'id' => $comment->id ], "Comment removed successfully.");
    }
}
