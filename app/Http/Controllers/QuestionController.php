<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Http\Requests\QuestionUpdateRequest;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use App\Services\QuestionService;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    use ResponseTrait;

    private $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $filters = $request->all();
        if (!$request->user()->is_admin) {
            $filters['approved'] = 1;
        }
        $questions = $this->questionService->getAllQuestions($filters);
        return $this->apiResponse(QuestionResource::collection($questions), 'success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionRequest $request
     * @return JsonResponse
     */
    public function store(QuestionRequest $request): JsonResponse
    {
        $question = $this->questionService->create($request->user(), $request->validated());
        return $this->apiResponse(new QuestionResource($question), 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return JsonResponse
     */
    public function show(Question $question): JsonResponse
    {
        $question->loadMissing('user');
        return $this->apiResponse(new QuestionResource($question), 'Question found.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param QuestionUpdateRequest $request
     * @param Question $question
     * @return JsonResponse
     */
    public function update(QuestionUpdateRequest $request, Question $question): JsonResponse
    {
        $this->authorize('update', $question);
        $updated = $this->questionService->update($question->id, $request->validated());
        return $this->apiResponse(new QuestionResource($question), 'Question updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return JsonResponse
     */
    public function destroy(Question $question): JsonResponse
    {
        $this->authorize('delete', $question);
        $deleted = $this->questionService->delete($question->id);
        return $this->apiResponse([], "Question removed successfully.");
    }
}
