<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\QuestionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API routes
Route::group([ 'middleware' => ['cors', 'json.response']], function () {
    // Guest routes
    Route::name('register')->post('register', [AuthController::class, 'register']);

    // Authenticated routes
    Route::group([ 'middleware' => ['auth:api']], function () {
        Route::name('user')->get('user', [AuthController::class, 'user']);
        Route::name('logout')->post('logout', [AuthController::class, 'logout']);
        Route::name('questions')->apiResource('questions', QuestionController::class);
        Route::name('comments')->apiResource('comments', CommentController::class);
    });
});
